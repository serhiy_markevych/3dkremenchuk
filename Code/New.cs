﻿using System;


public class New
{
    public int NewId { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
    public string ImageUrl { get; set; }
    public string Author { get; set; }
    public DateTime DateTime { get; set; } 
}
